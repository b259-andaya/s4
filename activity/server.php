<?php
session_start();

// Check if form is submitted
if (isset($_POST['email']) && isset($_POST['password'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];

    // Check if email and password are correct
    if ($email === 'johnsmith@gmail.com' && $password === '1234') {
        $_SESSION['email'] = $email;
        header('Location: dashboard.php');
        exit();
    } else {
        $_SESSION['error'] = 'Invalid username or password';
        header('Location: index.php');
        exit();
    }
}

// Check if logout is clicked
if (isset($_GET['logout'])) {
    session_destroy();
    header('Location: index.php');
    exit();
}
