<!DOCTYPE html>
<html>

<head>
    <title>Dashboard</title>
</head>

<body>
    <?php
    session_start();
    if (!isset($_SESSION['email'])) {
        header("location: index.php");
    }
    ?>
    <h2>Welcome <?php echo $_SESSION['email']; ?></h2>
    <a href="server.php?logout=true">Logout</a>
</body>

</html>