<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>s04 Activity: Client-Server Communication (Basic To-Do List)</title>
</head>

<body>
    <?php
    session_start();
    if (isset($_SESSION['email'])) {
        header("location: dashboard.php");
    }
    ?>

    <?php
    if (isset($_SESSION['error'])) {
        echo "<p>{$_SESSION['error']}</p>";
        unset($_SESSION['error']);
    }
    ?>

    <h2>Login Form</h2>
    <form method="post" action="server.php">
        <label>Email:</label>
        <input type="email" name="email" required><br><br>
        <label>Password:</label>
        <input type="password" name="password" required><br><br>
        <input type="submit" value="Login">
    </form>
</body>

</html>